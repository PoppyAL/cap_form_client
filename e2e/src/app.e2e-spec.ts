import { browser, by, element, ExpectedConditions } from 'protractor';
import { AppPage } from './app.po';

/*describe('new App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should be blank', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toContain('Start with Ionic UI Components');
  });
});*/

describe('new App', () => {

  beforeEach(() => {
    browser.get('/')
  });

  it('should display h1 titles', () => {

    let h1Elements = element.all(by.css('app-root h1'));
    expect(h1Elements.get(0).getText()).toEqual('A propos');
    expect(h1Elements.get(1).getText()).toEqual('Des questions?');
    });

  it('should display h2 title', () => {
    expect(element(by.css('app-root h2')).getText()).toEqual('C\'est parti !');
    });

  it('should open alert dialog, login as "Cédric" and go to EAPA-home', () => {
    element(by.buttonText('Connexion')).click();
    element(by.id('username')).sendKeys('cedric');
    element(by.id('password')).sendKeys('cedric');
    element(by.buttonText('Valider')).click();
    browser.waitForAngular();
    expect(browser.getCurrentUrl()).toEqual('http://localhost:4200/eapa-home');
  })
 });


