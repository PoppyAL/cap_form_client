import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'contact',
    loadChildren: () => import('./pages/contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'eapa-home',
    loadChildren: () => import('./pages/eapa-home/eapa-home.module').then( m => m.EapaHomePageModule)
  },
  {
    path: 'seniors',
    loadChildren: () => import('./pages/seniors/seniors.module').then( m => m.SeniorsPageModule)
  },
  {
    path: 'senior/byId/:id',
    loadChildren: () => import('./pages/senior/senior.module').then( m => m.SeniorPageModule)
  },
  {
    path: 'new-user',
    loadChildren: () => import('./pages/new-user/new-user.module').then( m => m.NewUserPageModule)
  },
  {
    path: 'new-session',
    loadChildren: () => import('./pages/new-session/new-session.module').then( m => m.NewSessionPageModule)
  },
  {
    path: 'session/byId/:id',
    loadChildren: () => import('./pages/session/session.module').then( m => m.SessionPageModule)
  },
  {
    path: 'sessions',
    loadChildren: () => import('./pages/sessions/sessions.module').then( m => m.SessionsPageModule)
  },
  {
    path: 'test6-mm/userId/:id',
    loadChildren: () => import('./pages/test6-mm/test6-mm.module').then( m => m.Test6MMPageModule)
  },
  {
    path: 'test-sppb/userId/:id',
    loadChildren: () => import('./pages/test-sppb/test-sppb.module').then( m => m.TestSppbPageModule)
  },
  {
    path: 'test2-mm/userId/:id',
    loadChildren: () => import('./pages/test2-mm/test2-mm.module').then( m => m.Test2MMPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'doc-home',
    loadChildren: () => import('./pages/doc-home/doc-home.module').then( m => m.DocHomePageModule)
  },
  {
    path: 'doctors',
    loadChildren: () => import('./pages/doctors/doctors.module').then( m => m.DoctorsPageModule)
  },
  {
    path: 'new-doctor',
    loadChildren: () => import('./pages/new-doctor/new-doctor.module').then( m => m.NewDoctorPageModule)
  },
  {
    path: 'test/byId/:id',
    loadChildren: () => import('./pages/test/test.module').then( m => m.TestPageModule)
  },
  {
    path: 'doctor/byId/:id',
    loadChildren: () => import('./pages/doctor/doctor.module').then( m => m.DoctorPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
