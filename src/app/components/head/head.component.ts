import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-head',
  templateUrl: './head.component.html',
  styleUrls: ['./head.component.scss'],
})
export class HeadComponent implements OnInit {

  constructor(
    public alertController: AlertController,
    private router: Router) { }

  ngOnInit() {}

  async presentAlertPrompt() {
    const alert = await this.alertController.create({
      cssClass: 'alertPrompt',
      header: 'Déconnexion',
      message: "Etes vous sûr ?",
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Oui',
          handler: () => {
            this.signOut();
          }
        }
      ]
    });
    await alert.present();
  }

  signOut(){
    localStorage.clear();
    let navigationExtras: NavigationExtras = {
      state: {
        token: false,
      }
    };
    this.router.navigate(['home'], navigationExtras);
  }

}
