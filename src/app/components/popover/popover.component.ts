import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NavParams, PopoverController, ToastController } from '@ionic/angular';

import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.component.html',
  styleUrls: ['./popover.component.scss'],
})
export class PopoverComponent implements OnInit {

  testName: string;

  constructor(private popover: PopoverController,
    public navParams:NavParams,
    public testService: TestService,
    public router: Router,
    public toastCtrl: ToastController) {}

  ngOnInit() {}

  validateTest(){
     this.popover.dismiss();
  }

  editTest(test){
    if(test.testName.label == "SPPB"){
    this.testName = 'test-sppb'
    } else if (test.testName.label == "2MM"){
      this.testName = 'test2-mm'
    } else if (test.testName.label == "6MM"){
      this.testName = "test6-mm"
    } else {
      this.testName = "seniors"
    }
    let navigationExtras: NavigationExtras = {
      state: {
        update: true,
        test: test
      }
    };
    this.router.navigate([this.testName + '/userId/' + test.senior.id], navigationExtras);
    this.popover.dismiss();
  }

  async deleteTest(senior, test){
    console.log(senior);
    console.log(test);
    for (let i=0 ; i<test.testItemScore.length ; i++){
      console.log(test.testItemScore[i].id);
      this.testService.deleteTestItemScore(test.testItemScore[i].id).subscribe();
    }

    console.log(test.id);
    await this.testService.deleteTest(test.id).subscribe();
    //location.reload();

    this.popover.dismiss();
    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été supprimé !',   
      duration: 3500,
    });  
    toast.present();
   }

}
