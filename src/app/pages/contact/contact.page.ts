import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import { ContactService } from './contact.service'

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  contactForm: any;

  constructor(public formBuilder: FormBuilder, 
    private contactService : ContactService,  
    private router: Router, 
    public toastCtrl: ToastController) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      tel: '',
      name: '',
      message: '',
    })
  }

  async submit(){
    this.contactService.saveContact(this.contactForm.value).subscribe(() => this.router.navigateByUrl('home'));
    const toast = await this.toastCtrl.create({  
      message: 'Votre message a bien été envoyé, nous vous répondra dans les plus bref délais !',   
      duration: 3500,
    });  
    toast.present();
  }

}
