import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  constructor(private httpClient: HttpClient) { }

  saveContact(newContact){
    return this.httpClient.post("http://localhost:3000/contact/new", newContact);

  }
}
