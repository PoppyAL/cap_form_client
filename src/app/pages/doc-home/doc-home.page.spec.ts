import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DocHomePage } from './doc-home.page';

describe('DocHomePage', () => {
  let component: DocHomePage;
  let fixture: ComponentFixture<DocHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DocHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
