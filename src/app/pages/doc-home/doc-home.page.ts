import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-doc-home',
  templateUrl: './doc-home.page.html',
  styleUrls: ['./doc-home.page.scss'],
})
export class DocHomePage implements OnInit {

  seniorsList$: Observable<any>;
  homesList$: Observable<any>;
  userSelect: any;
  usersByHome$: Observable<any>;
  newList:any;
  userId: any;
  userGroup: any;
  docName: string;

  constructor(private userService: UserService, 
    private router: Router) {
      this.userGroup = localStorage.getItem("userGroup");
      if(this.userGroup != 4){
        this.router.navigate(['home']);
        return;
      }
      this.userId = localStorage.getItem("userId");
      this.docName = localStorage.getItem("userLastName");
      this.seniorsList$ = this.userService.getSeniorsByDoc(this.userId);
      this.homesList$ = this.userService.getHomes();
      this.selectSeniorsByHome("Tous");
    }

  ngOnInit() {
    this.newList = this.seniorsList$;
  }

  selectSeniorsByHome(id){
    if (id === "Tous"){
      this.newList = this.seniorsList$;
      this.usersByHome$ = this.newList;
      return this.newList;
    } else {
    this.newList = this.userService.getSeniorsByHome(id);
    this.usersByHome$ = this.newList;
    return this.newList;
    }
  }

  seniorDetails(seniorSelected){
    this.router.navigate(['senior/byId/'+seniorSelected.id]);
  }  

  getItems(ev: any) {
    const val: string = ev.target.value;
    if (val && val.trim() !== '') {
       this.newList = this.usersByHome$
        .pipe(map((users) => 
        users.filter(user => (user.lastName.toLowerCase().indexOf(val.trim().toLowerCase()) !== -1) || (user.firstName.toLowerCase().indexOf(val.toLowerCase()) !== -1)) ));
        this.newList.subscribe(res => console.log(res));
    } else {
      this.newList = this.seniorsList$;
    }
  }
}
