import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-doctor',
  templateUrl: './doctor.page.html',
  styleUrls: ['./doctor.page.scss'],
})
export class DoctorPage implements OnInit {

  docId: any;
  doc$: any;
  patientsList$ : any;
  type: string;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private userService: UserService) {
      this.docId = this.route.snapshot.paramMap.get('id');
      this.type = "details";
  }

  ngOnInit() {
    this.doc$ = this.userService.getById(this.docId).subscribe((res) =>
      this.doc$ = res);
    this.patientsList$ = this.userService.getSeniorsByDoc(this.docId).subscribe((res) =>
      this.patientsList$ = res);
  }

  patientDetails(patient){
    this.router.navigate(['senior/byId/'+patient.id]);
  }

}
