import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.page.html',
  styleUrls: ['./doctors.page.scss'],
})
export class DoctorsPage implements OnInit {

  docList$: Observable<any>;
  docSelect: any;
  newList:any;

  constructor(private userService: UserService, 
    private router: Router) {
      this.docList$ = this.userService.getDoctors();
    }

  ngOnInit() {
    this.newList = this.docList$;
  }


  docDetails(docSelected){
    this.router.navigate(['doctor/byId/'+ docSelected.id]);
  }  

  getItems(ev: any) {
    const val: string = ev.target.value;
    if (val && val.trim() !== '') {
       this.newList = this.docList$
        .pipe(map((users) => 
        users.filter(user => (user.lastName.toLowerCase().indexOf(val.trim().toLowerCase()) !== -1) || (user.firstName.toLowerCase().indexOf(val.toLowerCase()) !== -1)) ));
        this.newList.subscribe(res => console.log(res));
    } else {
      this.newList = this.docList$;
    }
  }

}
