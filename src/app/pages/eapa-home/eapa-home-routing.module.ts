import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EapaHomePage } from './eapa-home.page';
import { HomePage } from '../home/home.page';

const routes: Routes = [
  {
    path: '',
    component: EapaHomePage
  },
  {
    path: '/home',
    component: HomePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EapaHomePageRoutingModule {}
