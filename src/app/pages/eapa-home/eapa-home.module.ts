import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EapaHomePageRoutingModule } from './eapa-home-routing.module';

import { EapaHomePage } from './eapa-home.page';
import { HeadComponent } from '../../components/head/head.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EapaHomePageRoutingModule
  ],
  declarations: [EapaHomePage, HeadComponent]
})
export class EapaHomePageModule {}


