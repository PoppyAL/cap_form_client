import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EapaHomePage } from './eapa-home.page';

describe('EapaAccueilPage', () => {
  let component: EapaHomePage;
  let fixture: ComponentFixture<EapaHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EapaHomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EapaHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
