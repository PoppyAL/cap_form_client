import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, RouterLink } from '@angular/router';
import { IonSelect } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-eapa-home',
  templateUrl: './eapa-home.page.html',
  styleUrls: ['./eapa-home.page.scss'],
})
export class EapaHomePage implements OnInit {

  userGroup: string;
  userId: string;
  user: any;
  userFirstName: string;

  constructor(private router: Router,
    private userService: UserService) { }

  ngOnInit() {
    this.userGroup = localStorage.getItem("userGroup");
    if(this.userGroup != "1"){
      this.router.navigate(['home']);
      return
    }
    this.userFirstName = localStorage.getItem("userFirstName");
  }
}
