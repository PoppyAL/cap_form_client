import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  userForm: any;
  userId: any;
  userGroup: any;
  user: any;
  loginToken: any;
  loggedIn: boolean = false;
  token: string;

  errorLoggin: string = '';

  constructor(public formBuilder: FormBuilder,
    public userService: UserService,
    private router: Router,
    public alertController: AlertController,
    private route: ActivatedRoute) {
      this.route.queryParams.subscribe(() => {
        if(this.router.getCurrentNavigation().extras.state){
          this.loggedIn = this.router.getCurrentNavigation().extras.state.token;
        }
      });
    }


  ngOnInit() {
    localStorage.getItem('token');
    if (this.token){
      this.loggedIn = true;
    }
  }

  async presentAlertPrompt() {
    this.errorLoggin = '';
    const alert = await this.alertController.create({
      cssClass: 'alertPrompt',
      header: 'Identification',
      inputs: [
        {
          name: 'username',
          type: 'text',
          placeholder: 'Pseudo',
          id: 'username'
        },
        {
          name: 'password',
          type: 'password',
          placeholder: 'Mot de passe',
          id: "password"
        },
      ],
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'secondary',
        }, {
          text: 'Valider',
          handler: (authData) => {
            this.signIn(authData.username, authData.password);
          }
        }
      ]
    });
    await alert.present();
  }

  goToHomeByUser(){
    this.userGroup = localStorage.getItem("userGroup");
    if (this.userGroup == "1"){
      this.router.navigate(['eapa-home']);
    } else if (this.userGroup == "4"){
      this.userId = localStorage.getItem("userId");
      this.router.navigate(['doctor/byId/'+this.userId]);
    }
  }

  async signIn(username, password){
    if (username == '' || password == ''){
      this.errorLoggin = "Identification incorrecte";
    } else if (username && password){
      this.userForm = this.formBuilder.group({
        userName: username,
        password: password
      })
    }
    this.userService.login(this.userForm.value).subscribe((res) => {
        this.loginToken = res;
        localStorage.setItem("token", this.loginToken.accessToken);
        this.token = localStorage.getItem('token');

        if (this.token) {
          console.log("token ok")
          this.userService.getByUsername(username).subscribe(res => {
            this.user = res;
            this.userId = this.user.id;
            this.userGroup = this.user.userGroup.id;
            localStorage.setItem("userId", this.userId);
            localStorage.setItem("userGroup", this.userGroup);
            localStorage.setItem("userFirstName", this.user.firstName);
            localStorage.setItem("userLastName", this.user.lastName);
            
            if(this.userGroup == 1){
              this.loggedIn = true;
              this.router.navigate(['eapa-home']);
            } else if (this.userGroup == 4 ){
              this.loggedIn = true;
              this.router.navigate(['doc-home']);
            }
          });
        }
      }, (error) => {
        this.errorLoggin = "Identification incorrecte";
      })
  }


}
