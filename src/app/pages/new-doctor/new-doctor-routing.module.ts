import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewDoctorPage } from './new-doctor.page';

const routes: Routes = [
  {
    path: '',
    component: NewDoctorPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewDoctorPageRoutingModule {}
