import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewDoctorPageRoutingModule } from './new-doctor-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { NewDoctorPage } from './new-doctor.page';
import { HeadComponent } from '../../components/head/head.component'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewDoctorPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [NewDoctorPage, HeadComponent]
})
export class NewDoctorPageModule {}
