import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NewDoctorPage } from './new-doctor.page';

describe('NewDoctorPage', () => {
  let component: NewDoctorPage;
  let fixture: ComponentFixture<NewDoctorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewDoctorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NewDoctorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
