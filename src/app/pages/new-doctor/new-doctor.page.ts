import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-new-doctor',
  templateUrl: './new-doctor.page.html',
  styleUrls: ['./new-doctor.page.scss'],
})
export class NewDoctorPage implements OnInit {

	newDoc: FormGroup;

  constructor(public formBuilder: FormBuilder, 
    private userService : UserService, 
    private router: Router, 
    public toastCtrl: ToastController) {

    this.newDoc = formBuilder.group({
        "firstName": [''],
        "lastName": [''],
        "tel": [''],
        "userGroup": 4
    });
  }

  ngOnInit() {
  }

  async onSubmit(){
    console.log(this.newDoc.value);
    this.userService.saveNewUser(this.newDoc.value).subscribe((res) => this.router.navigateByUrl('doctors'));
    const toast = await this.toastCtrl.create({  
      message: 'Le nouveau médecin a bien été rajouté !',   
      duration: 3500,
    });  
    toast.present();
  }

}
