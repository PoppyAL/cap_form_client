import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { NewSessionPageRoutingModule } from './new-session-routing.module';

import { NewSessionPage } from './new-session.page';
import { HeadComponent } from '../../components/head/head.component'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewSessionPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [NewSessionPage, HeadComponent]
})
export class NewSessionPageModule {}
