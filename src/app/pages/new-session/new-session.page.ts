import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-new-session',
  templateUrl: './new-session.page.html',
  styleUrls: ['./new-session.page.scss'],
})
export class NewSessionPage implements OnInit {

  themelist$: Observable<any>;
  detailslist$: Observable<any>;
  eapaList$: Observable<any>;
  
  sessionForm: any;
  details: FormArray;
  participents: FormArray;

  seniorsList$: Observable<any>;
  homesList$: Observable<any>;
  userSelect: any;
  usersByHome$: Observable<any>;

  constructor(public formBuilder: FormBuilder, 
    private sessionService : SessionService, 
    private userService : UserService, 
    private router: Router, 
    public toastCtrl: ToastController) {}

  ngOnInit(): void {
    this.themelist$ = this.sessionService.getThemes();
    this.detailslist$ = this.sessionService.getDetails();
    this.eapaList$ = this.userService.getEapa();

    this.seniorsList$ = this.userService.getSeniors();
    this.homesList$ = this.userService.getHomes();
    this.selectSeniorsByHome("Tous");
    
    this.sessionForm = this.formBuilder.group({
      date: '',
      eapa: '',
      theme: '',
      details: this.formBuilder.array([ ]),
      participent: this.formBuilder.array([ ])
    })
  }

  selectSeniorsByHome(id){
    if (id === "Tous"){
      this.usersByHome$ = this.seniorsList$;
      return this.usersByHome$;
    } else {
    this.usersByHome$ = this.userService.getSeniorsByHome(id);
    return this.usersByHome$.subscribe();
    }
  }

  addPart(id): void{
    this.participents = this.sessionForm.get('participent') as FormArray;
    this.participents.push(
      this.formBuilder.group({
        id: id
      })
    );
  }

  addDetail(detailsId){
    this.details = this.sessionForm.get('details') as FormArray;
    for (let i=0 ; i<detailsId.length ; i++){
      this.details.push(    
        this.formBuilder.group({
          id: detailsId[i]
        })
      )
    }   
  }

  async submit(f) {
    let newSession = this.sessionService.saveNewSession(f.value).subscribe();
    this.router.navigate(['accueil-eapa']);
    const toast = await this.toastCtrl.create({  
      message: 'La session a bien été enregistrée !',   
      duration: 3500,
    });  
    toast.present();
  }
}
