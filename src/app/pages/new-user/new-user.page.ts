import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

import { UserService } from 'src/app/services/user.service';
import { async } from '@angular/core/testing';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.page.html',
  styleUrls: ['./new-user.page.scss'],
})
export class NewUserPage implements OnInit {

	public newUser: FormGroup;
  homesList$: Observable<any>;
  doctorsList$: Observable<any>;

  userGroupNb: string = "";
  type: string = "general";

  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';

  resp: any;

  constructor(public formBuilder: FormBuilder, 
    private userService : UserService, 
    private router: Router, 
    public toastCtrl: ToastController) {


  }

  ngOnInit() {
    this.newUser = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      userName: [''],
      password: [''],
      sexe: [null],
      birthDate: [null],
      retirementHome: [null],
      userGroup: ['', Validators.required],
      doctor: [null],
      tel: [null]
  });
    this.homesList$ = this.userService.getHomes();
    this.doctorsList$ = this.userService.getDoctors();
  }

  selectUserGroup(value){
    this.userGroupNb = value;
  }

  hideShowPassword() {
      this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
      this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }

  async submit(){
    if (this.userGroupNb == "1" || this.userGroupNb == "4"){
      this.userService.register(this.newUser.value).subscribe(async (res) => {
        this.resp = res;
        if (this.resp.success == true){
          const toast = await this.toastCtrl.create({  
            message: this.resp.message,   
            duration: 3500,
          });  
          toast.present();
          this.router.navigate(['eapa-home']);
        }
      })
    } else if (this.userGroupNb == "2"){
      this.resp = this.userService.saveNewUser(this.newUser.value).subscribe(async (res) => {
        this.resp = res;
        if (!this.resp){
          const toast = await this.toastCtrl.create({  
            message: this.resp.message,   
            duration: 3500,
        });  
        toast.present();
        }
      })
    }
  }

}


