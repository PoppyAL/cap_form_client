import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeniorPage } from './senior.page';

describe('SeniorPage', () => {
  let component: SeniorPage;
  let fixture: ComponentFixture<SeniorPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeniorPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeniorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
