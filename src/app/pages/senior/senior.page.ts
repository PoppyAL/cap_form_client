import { toBase64String } from '@angular/compiler/src/output/source_map';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ActionSheetController, PopoverController } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

import { PopoverComponent} from './../../components/popover/popover.component'

@Component({
  selector: 'app-senior',
  templateUrl: './senior.page.html',
  styleUrls: ['./senior.page.scss'],
})
export class SeniorPage implements OnInit {

  senior$: any;
  sessionSelect: any; 
  seniorId= '';
  type: string;
  retirementHomeName: any;
  nbSessions: any;
  
  constructor(private route: ActivatedRoute,
    private router: Router,
    private popover: PopoverController,
    private userService: UserService,
    public actionSheetController: ActionSheetController) {
      this.type = 'detail';
      this.seniorId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    console.log(this.seniorId);
    this.senior$ = this.userService.getById(this.seniorId).subscribe((res) => {
      this.senior$ = res,
      console.log(res),
      this.retirementHomeName = this.senior$.retirementHome.name
    });
    this.nbSessions = this.userService.getNbSessionByUserId(this.seniorId).subscribe((res) => this.nbSessions = res);
  }

  createPopover(test){
    console.log(test);
    this.popover.create({
      component: PopoverComponent,
      animated: true,
      showBackdrop:false,
      componentProps:{test: test}
  }).then((popoverElement)=>{
        popoverElement.present();
    })
  }

  testDetails(testSelected){
    console.log(testSelected);
    console.log(testSelected.id);
    this.router.navigate(['/test/byId/'+testSelected.id]);
  }

  sessionDetails(sessionSelected){
    this.router.navigate(['/session/byId/'+sessionSelected.id]);
  }

  newTest(testName){
    this.router.navigate([testName + '/userId/' + this.seniorId]);
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'TESTS',
      //cssClass: 'action-sheet',
      buttons: [{
        text: 'SPPB',
        icon: 'caret-forward',
        handler: () => {
          this.router.navigate(['test-sppb/userId/' + this.seniorId]);
        }
      }, {
        text: '6 min marche',
        icon: 'caret-forward',
        handler: () => {
          this.router.navigate(['test6-mm/userId/' + this.seniorId]);
        }
      }, {
        text: '2 min marche sur place',
        icon: 'caret-forward',
        handler: () => {
          this.router.navigate(['test2-mm/userId/' + this.seniorId]);
        }
      }]
    });
    await actionSheet.present();
  }

}

