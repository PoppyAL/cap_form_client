import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeniorsPage } from './seniors.page';

describe('SeniorsPage', () => {
  let component: SeniorsPage;
  let fixture: ComponentFixture<SeniorsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeniorsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeniorsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
