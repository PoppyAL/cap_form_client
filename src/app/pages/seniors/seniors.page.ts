import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-seniors',
  templateUrl: './seniors.page.html',
  styleUrls: ['./seniors.page.scss'],
})
export class SeniorsPage implements OnInit {
  
  seniorsList$: Observable<any>;
  homesList$: Observable<any>;
  userSelect: any;
  usersByHome$: Observable<any>;
  newList:any;
  searchByType: string = "nom";

  constructor(private userService: UserService, 
    private router: Router) {
      this.seniorsList$ = this.userService.getSeniors();
      this.homesList$ = this.userService.getHomes();
      this.selectSeniorsByHome("Tous");
    }

  ngOnInit() {
    this.newList = this.seniorsList$;
  }

  selectSeniorsByHome(id){
    if (id === "Tous"){
      this.newList = this.seniorsList$;
      this.usersByHome$ = this.newList;
      return this.newList;
    } else {
    this.newList = this.userService.getSeniorsByHome(id);
    this.usersByHome$ = this.newList;
    return this.newList;
    }
  }

  searchBy(value){
    this.searchByType = value;
    this.selectSeniorsByHome("Tous");
  }

  seniorDetails(seniorSelected){
    this.router.navigate(['senior/byId/'+seniorSelected.id]);
  }  

  getItems(ev: any) {
    const val: string = ev.target.value;
    if (val && val.trim() !== '') {
       this.newList = this.usersByHome$
        .pipe(map((users) => 
        users.filter(user => (user.lastName.toLowerCase().indexOf(val.trim().toLowerCase()) !== -1) || (user.firstName.toLowerCase().indexOf(val.toLowerCase()) !== -1)) ));
        this.newList.subscribe(res => console.log(res));
    } else {
      this.newList = this.seniorsList$;
    }
  }
}