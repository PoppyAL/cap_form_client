import { TestBed } from '@angular/core/testing';

import { SeniorsService } from './seniors.service';

describe('SeniorsService', () => {
  let service: SeniorsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeniorsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
