import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { SessionService } from 'src/app/services/session.service';


@Component({
  selector: 'app-session',
  templateUrl: './session.page.html',
  styleUrls: ['./session.page.scss'],
})
export class SessionPage implements OnInit {

  //@Input()
  session$: any;
  userSelect: any;
  sessionId= '';
  themeName : any;
  eapaFirstName: any;
  eapaLastName: any;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private sessionService: SessionService) {
      this.sessionId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit() { 
    this.session$ = this.sessionService.getSessionById(this.sessionId).subscribe((res) => {
      this.session$ = res,
      console.log(this.session$),
      this.themeName = this.session$.theme.label,
      this.eapaFirstName = this.session$.eapa.firstName,
      this.eapaLastName = this.session$.eapa.lastName
      }
    );
  }

  getSeniorDetails(seniorSelected){
    this.router.navigate(['senior/byId/'+seniorSelected.id]);
  }  

  deleteSession(session){
    this.sessionService.deleteSession(session)
  }

  editSession(session){
    this.router.navigate(['session/byId/'+session.id]);
  }

}
