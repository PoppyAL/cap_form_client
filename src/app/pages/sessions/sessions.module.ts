import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SessionsPageRoutingModule } from './sessions-routing.module';
import { SessionsPage } from './sessions.page';
import { HeadComponent } from '../../components/head/head.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SessionsPageRoutingModule,
  ],
  declarations: [SessionsPage, HeadComponent]
})
export class SessionsPageModule {}
