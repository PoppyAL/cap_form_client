import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';

import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-sessions',
  templateUrl: './sessions.page.html',
  styleUrls: ['./sessions.page.scss'],
})
export class SessionsPage implements OnInit {

  sessionsList$: Observable<any>;
  eapaList$: Observable<any>
  sessionsByEapa$: Observable<any>;
  sessionSelect: any;
  eapaSelc: boolean;

  constructor(private sessionService: SessionService,
    private userService: UserService,
    private router: Router) { }

  ngOnInit() {
    this.sessionsList$ = this.sessionService.getSessions();
    this.eapaList$ = this.userService.getEapa();
    this.selectSessionsByEapa("Tous");
  }

  selectSessionsByEapa(id){
    if (id === "Tous"){
      this.sessionsByEapa$ = this.sessionsList$;
      this.eapaSelc = false;
      return this.sessionsByEapa$;
    } else {
    this.sessionsByEapa$ = this.sessionService.getSessionsByEapa(id);
    this.eapaSelc = true;
    return this.sessionsByEapa$;
    }
  }

  async sessionDetails(sessionSelected){
    this.router.navigate(['session/byId/' + sessionSelected.id]);
  }

  newSession(){
    this.router.navigate(['new-session'])
  }
  
}
