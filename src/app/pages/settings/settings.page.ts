import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';

import { SessionService } from 'src/app/services/session.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {

  type = '';
  itemList$: Observable<any>;
  form: FormGroup;
  itemSelected: any;
  itemIdToEdit: number;

  constructor(private userService: UserService,
    private sessionService: SessionService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.type = "add";
  }

  selectItem(item){
    this.itemSelected = item;
    if (this.type == "edit"){
      if(item == "ehpad"){
        this.itemList$ = this.userService.getHomes();
      } else if (item == "theme"){
        this.itemList$ = this.sessionService.getThemes();
      } else if (item == "detail"){
        this.itemList$ = this.sessionService.getDetails();
      }
    }
  }

  newItem(value){
    if (this.itemSelected == "ehpad"){
      this.form = this.formBuilder.group({
        name: value,
      })
      console.log(this.form.value);
      this.userService.newHome(this.form.value).subscribe();

    } else if (this.itemSelected == "theme"){
      this.form = this.formBuilder.group({
        label: value,
      })
      this.sessionService.saveNewTheme(this.form.value).subscribe();

    } else if (this.itemSelected == "detail"){
      this.form = this.formBuilder.group({
        label: value,
      })
      this.sessionService.saveNewDetail(this.form.value).subscribe();
    }
  }

  itemToEditId(id){
    this.itemIdToEdit = id;
  }

  updateItem(value){
    console.log("SALUT");
    if (this.itemSelected == "ehpad"){
      this.form = this.formBuilder.group({
        name: value,
      })
      console.log(this.form.value);
      this.userService.updateHome(this.itemIdToEdit, this.form.value).subscribe();

    } else if (this.itemSelected == "theme"){
      this.form = this.formBuilder.group({
        label: value,
      })
      this.sessionService.updateTheme(this.itemIdToEdit, this.form.value).subscribe();

    } else if (this.itemSelected == "detail"){
      this.form = this.formBuilder.group({
        label: value,
      })
      this.sessionService.updateDetail(this.itemIdToEdit, this.form.value).subscribe();
    }
  }

  deleteItem(id){
    if (this.itemSelected == "ehpad"){
      this.userService.deleteHome(this.itemIdToEdit).subscribe();

    } else if (this.itemSelected == "theme"){
      this.sessionService.deleteTheme(this.itemIdToEdit).subscribe();

    } else if (this.itemSelected == "detail"){
      this.sessionService.deleteDetail(this.itemIdToEdit).subscribe();
    }
  }


}
