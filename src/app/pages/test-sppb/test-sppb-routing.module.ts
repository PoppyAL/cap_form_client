import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TestSppbPage } from './test-sppb.page';

const routes: Routes = [
  {
    path: '',
    component: TestSppbPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TestSppbPageRoutingModule {}
