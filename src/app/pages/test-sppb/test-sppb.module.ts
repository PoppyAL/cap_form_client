import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TestSppbPageRoutingModule } from './test-sppb-routing.module';

import { TestSppbPage } from './test-sppb.page';
import { HeadComponent } from '../../components/head/head.component';
import { TimerComponent } from '../../components/timer/timer.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TestSppbPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [TestSppbPage, HeadComponent, TimerComponent]
})
export class TestSppbPageModule {}
