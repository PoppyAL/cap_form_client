import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TestSppbPage } from './test-sppb.page';

describe('TestSppbPage', () => {
  let component: TestSppbPage;
  let fixture: ComponentFixture<TestSppbPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestSppbPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TestSppbPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
