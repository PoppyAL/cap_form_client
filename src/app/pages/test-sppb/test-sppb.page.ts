import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable, } from 'rxjs';

import { TestService } from 'src/app/services/test.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-test-sppb',
  templateUrl: './test-sppb.page.html',
  styleUrls: ['./test-sppb.page.scss'],
})
export class TestSppbPage implements OnInit {

  testForm: any;
  totalScore: number = 0;
  senior$: any;
  seniorId= '';
  
  update: boolean = false;
  testIdToUpdate = '';
  testItemsScore: FormArray;
  testToUpdate: any;
  eapaId: string;

  itemForm: any;

  constructor(public formBuilder: FormBuilder, 
    public testService: TestService,
    public userService: UserService,
    public router: Router,
    public toastCtrl: ToastController,
    private route: ActivatedRoute) { 
      this.route.queryParams.subscribe(() => {
        if(this.router.getCurrentNavigation().extras.state){
          this.testToUpdate = this.router.getCurrentNavigation().extras.state.test;
          this.update = this.router.getCurrentNavigation().extras.state.update;
        }
        this.eapaId = localStorage.getItem('userId');

      });
    }

  ngOnInit() {
    this.seniorId = this.route.snapshot.paramMap.get('id');
    this.senior$ = this.userService.getById(this.seniorId)
          .subscribe((res) => this.senior$ = res);

    this.testForm = this.formBuilder.group({
      date: '',
      scoreTest: '',
      testName: '1',
      senior: this.seniorId,
      eapa: this.eapaId,
      testItemScore : this.formBuilder.array([ ])
    });
    if (this.update){
      this.updateTestItem();
    }
  }

  updateTestItem(){
    this.testItemsScore = this.testForm.get('testItemScore') as FormArray;
    for (let i=0 ; i<this.testToUpdate.testItemScore.length ; i++){
      this.testItemsScore.push(    
        this.formBuilder.group({
          score: this.testToUpdate.testItemScore[i].score,
          testItem: this.testToUpdate.testItemScore[i].testItem.id
        })
      )
    }
    this.totalScore = this.testToUpdate.scoreTest;
    this.testForm.get('date').setValue(this.testToUpdate.date);
  }

  getValue(id){
    if (this.update){
      for (let i=0 ; i<this.testToUpdate.testItemScore.length ; i++){
        if(this.testToUpdate.testItemScore[i].testItem.id === id){
          return this.testToUpdate.testItemScore[i].score.toString();
        }
      }
    }
  }

  addTestItem(score: number, item): void{
    this.testItemsScore = this.testForm.get('testItemScore') as FormArray;
    for (let i=0 ; i<this.testItemsScore.length ; i++){
      if( this.testItemsScore.value[i].testItem == item){
        this.totalScore -= (+this.testItemsScore.value[i].score)
        this.testItemsScore.removeAt(i);
      }
    }
    this.testItemsScore.push(    
      this.formBuilder.group({
        score: score,
        testItem:item
      })
    )
    this.totalScore += (+score);
  }

  async submitAddTest(){
    this.testForm.get('scoreTest').setValue(this.totalScore);
    this.testService.saveNewTest(this.testForm.value).subscribe();
    
    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été enregistré !',   
      duration: 3500,
    });  
    toast.present();

    this.router.navigate(['seniors']);
  }

  async submitUpdateTest(){
    this.testForm.get('scoreTest').setValue(this.totalScore);
    const scoreTot = this.formBuilder.group({
      scoreTest: this.totalScore
    })
    this.testService.updateTest(this.testToUpdate.id, scoreTot.value).subscribe();

    for (let i=0 ; i<this.testItemsScore.length ; i++){
      console.log(this.testItemsScore.value[i].score);
      this.testService.updateTestItemScore(this.testToUpdate.testItemScore[i].id, this.testItemsScore.value[i])
            .subscribe();
    }

    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été mit à jour !',   
      duration: 3500,
    });  
    toast.present();

    this.router.navigate(['seniors']);
  }

}
