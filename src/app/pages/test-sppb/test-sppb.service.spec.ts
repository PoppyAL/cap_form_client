import { TestBed } from '@angular/core/testing';

import { TestSppbService } from './test-sppb.service';

describe('TestSppbService', () => {
  let service: TestSppbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TestSppbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
