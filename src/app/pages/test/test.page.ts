import { ResourceLoader } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { TestService } from 'src/app/services/test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})
export class TestPage implements OnInit {
  
  testName: string;
  testName2: any;
  eapa: any;
  testId: any;
  test: any;

  constructor(public testService: TestService,
    public router: Router,
    private route: ActivatedRoute,
    public toastCtrl: ToastController) {
      this.testId = this.route.snapshot.paramMap.get('id');
    }

  ngOnInit() {
    console.log(this.testId);
    this.test = this.testService.getTestById(this.testId).subscribe((res) => {
      this.test = res;
      this.testName2 = this.test.testName.label;
      this.eapa = this.test.eapa.firstName;
    });
  }

  editTest(test){
    if(test.testName.label == "SPPB"){
    this.testName = 'test-sppb'
    } else if (test.testName.label == "2MM"){
      this.testName = 'test2-mm'
    } else if (test.testName.label == "6MM"){
      this.testName = "test6-mm"
    } else {
      this.testName = "seniors"
    }
    let navigationExtras: NavigationExtras = {
      state: {
        update: true,
        test: test
      }
    };
    this.router.navigate([this.testName + '/userId/' + test.senior.id], navigationExtras);
  }

  async deleteTest(senior, test){
    /*for (let i=0 ; i<test.testItemScore.length ; i++){
      this.testService.deleteTestItemScore(test.testItemScore[i].id).subscribe();
    }*/
    await this.testService.deleteTest(test.id).subscribe();
    this.router.navigate(['seniors']);

    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été supprimé !',   
      duration: 3500,
    });  
    toast.present();
   }

}
