import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Test2MMPageRoutingModule } from './test2-mm-routing.module';

import { Test2MMPage } from './test2-mm.page';
import { HeadComponent } from '../../components/head/head.component';
import { TimerComponent } from '../../components/timer/timer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Test2MMPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [Test2MMPage, HeadComponent, TimerComponent]
})
export class Test2MMPageModule {}
