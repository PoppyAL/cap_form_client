import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Test2MMPage } from './test2-mm.page';

describe('Test2MMPage', () => {
  let component: Test2MMPage;
  let fixture: ComponentFixture<Test2MMPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Test2MMPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Test2MMPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
