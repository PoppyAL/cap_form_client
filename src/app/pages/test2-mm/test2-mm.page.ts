import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';

import { TestService } from 'src/app/services/test.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-test2-mm',
  templateUrl: './test2-mm.page.html',
  styleUrls: ['./test2-mm.page.scss'],
})
export class Test2MMPage implements OnInit {

  senior$: any;
  seniorId= '';
  testForm: any;
  eapaId: string;
  
  nbUps: number = 0;

  update: boolean = false;
  testIdToUpdate = '';
  testToUpdate: any;

  testItemsScore: FormArray;

  constructor(public formBuilder: FormBuilder, 
    public testService: TestService,
    public userService: UserService,
    public router: Router,
    public toastCtrl: ToastController,
    private route: ActivatedRoute) {
      this.route.queryParams.subscribe(() => {
        if(this.router.getCurrentNavigation().extras.state){
          this.testToUpdate = this.router.getCurrentNavigation().extras.state.test;
          this.update = this.router.getCurrentNavigation().extras.state.update;
        }
        this.eapaId = localStorage.getItem('userId');
      });
     }

    ngOnInit() {
      this.seniorId = this.route.snapshot.paramMap.get('id');
      this.senior$ = this.userService.getById(this.seniorId).subscribe((res) => this.senior$ = res);
      
      this.testForm = this.formBuilder.group({
        date: '',
        scoreTest: '',
        testName: '3',
        senior: this.seniorId,
        eapa: this.eapaId,
        testItemScore: this.formBuilder.array([])
      });

      if (this.update){
        this.updateTestItem();
      }
    }

    updateTestItem(){
      this.testItemsScore = this.testForm.get('testItemScore') as FormArray;
      for (let i=0 ; i<this.testToUpdate.testItemScore.length ; i++){
        this.testItemsScore.push(    
          this.formBuilder.group({
            score: this.testToUpdate.testItemScore[i].score,
            testItem: this.testToUpdate.testItemScore[i].testItem.id
          })
        )
      }
      this.nbUps = this.testToUpdate.scoreTest;
      this.testForm.get('date').setValue(this.testToUpdate.date);
    }

  addUp(){
    this.nbUps += 1;
  }

  removeUp(){
    this.nbUps -= 1;
  }

  clearUp(){
    this.nbUps = 0; 
  }

  addTestItem(score: number, item){
    this.testItemsScore = this.testForm.get('testItemScore') as FormArray;
    this.testItemsScore.push(    
      this.formBuilder.group({
        score: score,
        testItem:item
      })
    )
  }

  async submit(){
    this.addTestItem(this.nbUps, 14);
    this.testForm.get('scoreTest').setValue(this.nbUps);

    this.testService.saveNewTest(this.testForm.value).subscribe();

    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été rajouté !',   
      duration: 3500,
    });  
    toast.present();

    this.router.navigate(['seniors']);
  }


  async submitUpdate(){

    this.testForm.get('scoreTest').setValue(this.nbUps);

    this.testToUpdate.scoreTest = this.nbUps;
    this.testToUpdate.testItemScore.score = this.nbUps

    this.testService.updateTest(this.testToUpdate.id, this.testForm.value).subscribe();

    for (let i=0 ; i<this.testItemsScore.length ; i++){
      this.testService.updateTestItemScore(this.testToUpdate.testItemScore[i].id, this.testToUpdate.testItemScore[i]).subscribe();
    }

    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été mit à jour !',   
      duration: 3500,
    });  
    toast.present();

    this.router.navigate(['seniors']);
  }

}
