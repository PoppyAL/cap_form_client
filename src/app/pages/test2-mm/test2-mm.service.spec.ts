import { TestBed } from '@angular/core/testing';

import { Test2MMService } from './test2-mm.service';

describe('Test2MMService', () => {
  let service: Test2MMService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Test2MMService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
