import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Test6MMPage } from './test6-mm.page';

const routes: Routes = [
  {
    path: '',
    component: Test6MMPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Test6MMPageRoutingModule {}
