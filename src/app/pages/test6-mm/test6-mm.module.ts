import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';


import { Test6MMPageRoutingModule } from './test6-mm-routing.module';

import { Test6MMPage } from './test6-mm.page';
import { HeadComponent } from '../../components/head/head.component';
import { TimerComponent } from '../../components/timer/timer.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Test6MMPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [Test6MMPage, HeadComponent, TimerComponent]
})
export class Test6MMPageModule {}
