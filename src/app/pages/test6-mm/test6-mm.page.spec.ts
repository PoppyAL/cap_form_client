import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Test6MMPage } from './test6-mm.page';

describe('Test6MMPage', () => {
  let component: Test6MMPage;
  let fixture: ComponentFixture<Test6MMPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Test6MMPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Test6MMPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
