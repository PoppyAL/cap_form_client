import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { TestService } from 'src/app/services/test.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-test6-mm',
  templateUrl: './test6-mm.page.html',
  styleUrls: ['./test6-mm.page.scss'],
})
export class Test6MMPage implements OnInit {

  distance: number = 0;
  nbBreaks: number = 0;

  testForm: any;
  senior$: any;
  seniorId= '';
  eapaId: string;
  type: string;

  bfHr: number;
  bfDysp: number;
  bfPain: number;

  afHr: number;
  afDysp: number;
  afPain: number;

  testItemsScore: FormArray;

  update: boolean = false;
  testIdToUpdate = '';
  testToUpdate: any;


  constructor(public formBuilder: FormBuilder, 
    public testService: TestService,
    public userService: UserService,
    public router: Router,
    public toastCtrl: ToastController,
    private route: ActivatedRoute) {
      this.route.queryParams.subscribe(() => {
        if(this.router.getCurrentNavigation().extras.state){
          this.testToUpdate = this.router.getCurrentNavigation().extras.state.test;
          this.update = this.router.getCurrentNavigation().extras.state.update;
        }
        this.eapaId = localStorage.getItem('userId');
      });
    }

  ngOnInit() {
    this.seniorId = this.route.snapshot.paramMap.get('id');
    this.senior$ = this.userService.getById(this.seniorId).subscribe((res) => this.senior$ = res);

    this.testForm = this.formBuilder.group({
      date: '',
      scoreTest: '',
      testName: '2',
      senior: this.seniorId,
      eapa: this.eapaId,
      testItemScore: this.formBuilder.array([])
    });
    this.type = 'before';
  }

  addTestItem(score: number, item){
    this.testItemsScore = this.testForm.get('testItemScore') as FormArray;
    for (let i=0 ; i<this.testItemsScore.length ; i++){
      if( this.testItemsScore.value[i].testItem == item){
        this.testItemsScore.removeAt(i);
      }
    }
    this.testItemsScore.push(    
      this.formBuilder.group({
        score: score,
        testItem:item
      })
    )
    if(item == 8){
      this.bfHr = score;
    } else if (item == 9){
      this.bfDysp = score;
    } else if(item == 10){
      this.bfPain = score;
    }
  }

  distTot(newDist){
    this.distance = newDist;
  }

  addDist(){
    this.distance += 1;
  }

  removeDist(){
    this.distance -= 1;
  }

  addBreak(){
    this.nbBreaks += 1;
  }

  removeBreak(){
    this.nbBreaks -= 1;
  }

  async submit(){
    this.addTestItem(this.distance, 6);
    this.addTestItem(this.nbBreaks, 7);
    this.testForm.get('scoreTest').setValue(this.distance);

    this.testService.saveNewTest(this.testForm.value).subscribe();

    const toast = await this.toastCtrl.create({  
      message: 'Le test a bien été rajouté !',   
      duration: 3500,
    });  
    toast.present();

    this.router.navigate(['seniors']);
  }



}
