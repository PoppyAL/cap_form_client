import { TestBed } from '@angular/core/testing';

import { Test6MmService } from './test6-mm.service';

describe('Test6MmService', () => {
  let service: Test6MmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Test6MmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
