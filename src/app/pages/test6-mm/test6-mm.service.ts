import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Test6MmService {

  constructor(public http: HttpClient) { }

}
