import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor(private http: HttpClient) { }

  //GET
  getSessions(){
    return this.http.get("http://localhost:3000/sessions/all")
  }

  getSessionById(id){
    return this.http.get("http://localhost:3000/sessions/byId"+id);
  }

  getSessionsByEapa(id: number){
    return this.http.get("http://localhost:3000/sessions/sessionByEapa"+id)
  }

  getThemes(){
    return this.http.get("http://localhost:3000/session-theme/all");
  }

  getDetails(){
    return this.http.get("http://localhost:3000/session-theme-detail/all")
  }

  getEapa(){
    return this.http.get("http://localhost:3000/users/fromUserGroup1")
  }

  //NEW
  saveNewSession(newSession){
    return this.http.post("http://localhost:3000/sessions/new", newSession);
  }

  saveNewTheme(newTheme){
    return this.http.post("http://localhost:3000/session-theme/new", newTheme)
  }

  saveNewDetail(newDetail){
    return this.http.post("http://localhost:3000/session-theme-detail/new", newDetail)
  }

  //UPDATE
  updateSession(id, session){
    return this.http.put("http://localhost:3000/sessions/update"+id, session)
  }

  updateTheme(id, theme){
    return this.http.put("http://localhost:3000/sessions/update"+id, theme)
  }

  updateDetail(id, detail){
    return this.http.put("http://localhost:3000/sessions/update"+id, detail)
  }

  //DELETE
  deleteSession(sessionId){
    return this.http.delete("http://localhost:3000/sessions/delete"+sessionId);
  }

  deleteTheme(themeId){
    return this.http.delete("http://localhost:3000/session-theme/delete"+themeId);
  }

  deleteDetail(detailId){
    return this.http.delete("http://localhost:3000/session-theme-detail/delete"+detailId);
  }

}
