import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TestService {

  constructor(public http: HttpClient) { }

  //GET
  getTests(){
    return this.http.get("http://localhost:3000/test/new")
  }

  getTestById(id){
    return this.http.get("http://localhost:3000/test/byId"+id)
  }
  
  //NEW
  saveNewTest(newTest){
    return this.http.post("http://localhost:3000/test/new", newTest)
  }

  saveNewTestItem(newTestItem){
    return this.http.post("http://localhost:3000/test-item-score/new", newTestItem)
  }

  //UPDATE
  updateTest(id, test){
    return this.http.put("http://localhost:3000/test/update"+id, test)
  }

  updateTestItemScore(id: number, testItemScore){
    return this.http.put("http://localhost:3000/test-item-score/update"+id, testItemScore)
  }

  //DELETE
  deleteTest(id){
    return this.http.delete("http://localhost:3000/test/delete"+id);
  }

  deleteTestItemScore(id){
    return this.http.delete("http://localhost:3000/test-item-score/delete"+id);
  }

}
