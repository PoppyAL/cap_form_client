import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient ) {  }
  
  //AUTH
  login(user){
    return this.http.post("http://localhost:3000/auth/login", user)
  }

  register(user){
    return this.http.post("http://localhost:3000/auth/register", user)
  }

  //GET
  getSeniors(){
    return this.http.get("http://localhost:3000/users/fromUserGroup2")
  }

  getById(id){
    return this.http.get("http://localhost:3000/users/byId"+id);
  }

  getSeniorsByHome(id: number){
    return this.http.get("http://localhost:3000/users/fromHome"+id)
  }

  getSeniorsByDoc(id: number){
    return this.http.get("http://localhost:3000/users/byDoc"+id)
  }

  getEapa(){
    return this.http.get("http://localhost:3000/users/fromUserGroup1")
  }

  getDoctors(){
    return this.http.get("http://localhost:3000/users/fromUserGroup4")
  }

  getHomes(){
    return this.http.get("http://localhost:3000/retirementhome/all");
  }

  getByUsername(username: string){
    return this.http.get("http://localhost:3000/users/byUsername/"+username);
  }

  getHomeById(id: number){
    return this.http.get("http://localhost:3000/retirementhome/byId"+id)
  }

  getNbSessionByUserId(id){
    return this.http.get("http://localhost:3000/users/findNbSessionByUserId"+id)
  }

  //NEW
  newHome(newHome){
    return this.http.post("http://localhost:3000/retirementhome/new", newHome)
  }

  saveNewUser(newUser){
    console.log(newUser)
    return this.http.post("http://localhost:3000/users/new", newUser);
  }

  //UPDATE
  updateHome(id, newHome){
    return this.http.post("http://localhost:3000/retirementhome/update"+id, newHome)
  }

  //DELETE
  deleteHome(id){
    return this.http.delete("http://localhost:3000/retirementhome/delete"+id)
  }
}
